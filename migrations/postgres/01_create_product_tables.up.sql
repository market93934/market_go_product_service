CREATE TABLE "brand" (
  "id" UUID PRIMARY KEY ,
  "photo" varchar,
  "name" varchar(45),
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "category" (
  "id" UUID PRIMARY KEY,
  "name" varchar(45),
  "parent_id" UUID REFERENCES  "category"("id"),
  "brand_id" UUID REFERENCES  "brand"("id"),
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);



CREATE TABLE "product" (
  "id" UUID PRIMARY KEY ,
  "photo" varchar,
  "name" varchar(45),
  "category_id" UUID REFERENCES  "category"("id"),
  "brand_id" UUID REFERENCES  "brand"("id"),
  "bar_code" varchar unique,
  "price" NUMERIC ,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "reminder" (
  "branch_id" UUID  NOT NULL,
  "category_id" UUID  ,
  "brand_id" UUID  NOT NULL,
  "product_id" UUID  PRIMARY KEY,
  "bar_code" varchar unique,
  "count" int,
  "price" NUMERIC ,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

