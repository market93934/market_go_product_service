package service

import (
	"context"

	"gitlab.com/market93934/market_go_product_service/config"
	"gitlab.com/market93934/market_go_product_service/genproto/product_service"
	"gitlab.com/market93934/market_go_product_service/grpc/client"
	"gitlab.com/market93934/market_go_product_service/pkg/logger"
	"gitlab.com/market93934/market_go_product_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type brandService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*product_service.UnimplementedBrandServiceServer
}

func NewBrandService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *brandService {
	return &brandService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *brandService) Create(ctx context.Context, req *product_service.CreateBrand) (resp *product_service.Brand, err error) {

	i.log.Info("---CreateBrand------>", logger.Any("req", req))

	pKey, err := i.strg.Brand().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateBrand->Brand->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Brand().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyBrand->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *brandService) GetByID(ctx context.Context, req *product_service.BrandPrimaryKey) (resp *product_service.Brand, err error) {

	i.log.Info("---GetBrandByID------>", logger.Any("req", req))

	resp, err = i.strg.Brand().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetBrandByID->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *brandService) GetList(ctx context.Context, req *product_service.GetListBrandRequest) (*product_service.GetListBrandResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Brand().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListBrand->Brand->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *brandService) Update(ctx context.Context, req *product_service.UpdateBrand) (resp *product_service.Brand, err error) {
	i.log.Info("---UpdateBrand------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Brand().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateBrand--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Brand().GetByPKey(ctx, &product_service.BrandPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetBrand->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *brandService) Delete(ctx context.Context, req *product_service.BrandPrimaryKey) (resp *product_service.BrandEmpty, err error) {

	i.log.Info("---DeleteBrand------>", logger.Any("req", req))

	resp, err = i.strg.Brand().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteBrand->Brand->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
