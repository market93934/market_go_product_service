package service

import (
	"context"

	"gitlab.com/market93934/market_go_product_service/config"
	"gitlab.com/market93934/market_go_product_service/genproto/product_service"
	"gitlab.com/market93934/market_go_product_service/grpc/client"
	"gitlab.com/market93934/market_go_product_service/pkg/logger"
	"gitlab.com/market93934/market_go_product_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type reminderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*product_service.UnimplementedReminderServiceServer
}

func NewReminderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *reminderService {
	return &reminderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *reminderService) Create(ctx context.Context, req *product_service.CreateReminder) (resp *product_service.Reminder, err error) {

	i.log.Info("---CreateReminder------>", logger.Any("req", req))

	pKey, err := i.strg.Reminder().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateReminder->Reminder->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Reminder().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyReminder->Reminder->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *reminderService) GetByID(ctx context.Context, req *product_service.ReminderPrimaryKey) (resp *product_service.Reminder, err error) {

	i.log.Info("---GetReminderByID------>", logger.Any("req", req))

	resp, err = i.strg.Reminder().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetReminderByID->Reminder->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *reminderService) GetList(ctx context.Context, req *product_service.GetListReminderRequest) (*product_service.GetListReminderResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Reminder().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListReminder->Reminder->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *reminderService) Update(ctx context.Context, req *product_service.UpdateReminder) (resp *product_service.Reminder, err error) {
	i.log.Info("---UpdateReminder------>", logger.Any("req", req))

	reminder, err := i.strg.Reminder().GetByPKey(ctx, &product_service.ReminderPrimaryKey{
		ProductId: req.ProductId,
	})
	if err != nil {
		i.log.Error("!!!UpdateReminder--->GetById", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	if reminder.Count > req.Count {

		rowsAffected, err := i.strg.Reminder().Update(ctx, req)

		if err != nil {
			i.log.Error("!!!UpdateReminder--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		if rowsAffected <= 0 {
			return nil, status.Error(codes.InvalidArgument, "no rows were affected")
		}

		resp, err = i.strg.Reminder().GetByPKey(ctx, &product_service.ReminderPrimaryKey{ProductId: req.ProductId})
		if err != nil {
			i.log.Error("!!!GetReminder->Reminder->Get--->", logger.Error(err))
			return nil, status.Error(codes.NotFound, err.Error())
		}
	}
	return resp, err
}

func (i *reminderService) Delete(ctx context.Context, req *product_service.ReminderPrimaryKey) (resp *product_service.ReminderEmpty, err error) {

	i.log.Info("---DeleteReminder------>", logger.Any("req", req))

	resp, err = i.strg.Reminder().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteReminder->Reminder->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
