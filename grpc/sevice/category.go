package service

import (
	"context"

	"gitlab.com/market93934/market_go_product_service/config"
	"gitlab.com/market93934/market_go_product_service/genproto/product_service"
	"gitlab.com/market93934/market_go_product_service/grpc/client"
	"gitlab.com/market93934/market_go_product_service/pkg/logger"
	"gitlab.com/market93934/market_go_product_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type categoryService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*product_service.UnimplementedCategoryServiceServer
}

func NewCategoryService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *categoryService {
	return &categoryService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *categoryService) Create(ctx context.Context, req *product_service.CreateCategory) (resp *product_service.Category, err error) {

	i.log.Info("---CreateCategory------>", logger.Any("req", req))

	pKey, err := i.strg.Category().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateCategory->Category->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Category().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *categoryService) GetByID(ctx context.Context, req *product_service.CategoryPrimaryKey) (resp *product_service.Category, err error) {

	i.log.Info("---GetCategoryByID------>", logger.Any("req", req))

	resp, err = i.strg.Category().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetCategoryByID->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *categoryService) GetList(ctx context.Context, req *product_service.GetListCategoryRequest) (*product_service.GetListCategoryResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Category().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListCategory->Category->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *categoryService) Update(ctx context.Context, req *product_service.UpdateCategory) (resp *product_service.Category, err error) {
	i.log.Info("---UpdateCategory------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Category().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateCategory--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Category().GetByPKey(ctx, &product_service.CategoryPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *categoryService) Delete(ctx context.Context, req *product_service.CategoryPrimaryKey) (resp *product_service.CategoryEmpty, err error) {

	i.log.Info("---DeleteCategory------>", logger.Any("req", req))

	resp, err = i.strg.Category().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteCategory->Category->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
