package service

import (
	"context"

	"gitlab.com/market93934/market_go_product_service/config"
	"gitlab.com/market93934/market_go_product_service/genproto/product_service"
	"gitlab.com/market93934/market_go_product_service/grpc/client"
	"gitlab.com/market93934/market_go_product_service/pkg/logger"
	"gitlab.com/market93934/market_go_product_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type productService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*product_service.UnimplementedProductServiceServer
}

func NewProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *productService {
	return &productService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *productService) Create(ctx context.Context, req *product_service.CreateProduct) (resp *product_service.Product, err error) {

	i.log.Info("---CreateProduct------>", logger.Any("req", req))

	pKey, err := i.strg.Product().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateProduct->Product->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Product().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *productService) GetByID(ctx context.Context, req *product_service.ProductPrimaryKey) (resp *product_service.Product, err error) {

	i.log.Info("---GetProductByID------>", logger.Any("req", req))

	resp, err = i.strg.Product().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetProductByID->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *productService) GetList(ctx context.Context, req *product_service.GetListProductRequest) (*product_service.GetListProductResponse, error) {
	i.log.Info("-------GetListduser-------", logger.Any("req", req))

	resp, err := i.strg.Product().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListProduct->Product->Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *productService) Update(ctx context.Context, req *product_service.UpdateProduct) (resp *product_service.Product, err error) {
	i.log.Info("---UpdateProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Product().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Product().GetByPKey(ctx, &product_service.ProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *productService) Delete(ctx context.Context, req *product_service.ProductPrimaryKey) (resp *product_service.ProductEmpty, err error) {

	i.log.Info("---DeleteProduct------>", logger.Any("req", req))

	resp, err = i.strg.Product().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
