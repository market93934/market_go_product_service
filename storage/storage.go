package storage

import (
	"context"

	"gitlab.com/market93934/market_go_product_service/genproto/product_service"
)

type StorageI interface {
	CloseDB()
	Product() ProductRepoI
	Brand() BrandRepoI
	Category() CategoryRepoI
	Reminder() ReminderRepoI
}

type ProductRepoI interface {
	Create(context.Context, *product_service.CreateProduct) (*product_service.ProductPrimaryKey, error)
	GetByPKey(context.Context, *product_service.ProductPrimaryKey) (*product_service.Product, error)
	GetAll(context.Context, *product_service.GetListProductRequest) (*product_service.GetListProductResponse, error)
	Update(context.Context, *product_service.UpdateProduct) (int64, error)
	Delete(context.Context, *product_service.ProductPrimaryKey) (*product_service.ProductEmpty, error)
}

type BrandRepoI interface {
	Create(context.Context, *product_service.CreateBrand) (*product_service.BrandPrimaryKey, error)
	GetByPKey(context.Context, *product_service.BrandPrimaryKey) (*product_service.Brand, error)
	GetAll(context.Context, *product_service.GetListBrandRequest) (*product_service.GetListBrandResponse, error)
	Update(context.Context, *product_service.UpdateBrand) (int64, error)
	Delete(context.Context, *product_service.BrandPrimaryKey) (*product_service.BrandEmpty, error)
}
type CategoryRepoI interface {
	Create(context.Context, *product_service.CreateCategory) (*product_service.CategoryPrimaryKey, error)
	GetByPKey(context.Context, *product_service.CategoryPrimaryKey) (*product_service.Category, error)
	GetAll(context.Context, *product_service.GetListCategoryRequest) (*product_service.GetListCategoryResponse, error)
	Update(context.Context, *product_service.UpdateCategory) (int64, error)
	Delete(context.Context, *product_service.CategoryPrimaryKey) (*product_service.CategoryEmpty, error)
}

type ReminderRepoI interface {
	Create(context.Context, *product_service.CreateReminder) (*product_service.ReminderPrimaryKey, error)
	GetByPKey(context.Context, *product_service.ReminderPrimaryKey) (*product_service.Reminder, error)
	GetAll(context.Context, *product_service.GetListReminderRequest) (*product_service.GetListReminderResponse, error)
	Update(context.Context, *product_service.UpdateReminder) (int64, error)
	Delete(context.Context, *product_service.ReminderPrimaryKey) (*product_service.ReminderEmpty, error)
}
