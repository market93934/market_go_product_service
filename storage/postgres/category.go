package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_product_service/genproto/product_service"
	"gitlab.com/market93934/market_go_product_service/pkg/helper"
)

type categoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) *categoryRepo {
	return &categoryRepo{
		db: db,
	}
}

func (c *categoryRepo) Create(ctx context.Context, req *product_service.CreateCategory) (resp *product_service.CategoryPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "category" (
			id,
			brand_id,
			name,
			parent_id
			) VALUES ($1, $2, $3, $4)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BrandId,
		req.Name,
		helper.NewNullString(req.ParentId),
	)
	if err != nil {
		return nil, err
	}

	return &product_service.CategoryPrimaryKey{Id: id.String()}, nil

}

func (c *categoryRepo) GetByPKey(ctx context.Context, req *product_service.CategoryPrimaryKey) (resp *product_service.Category, err error) {
	query := `
		SELECT
			id,
			name,
			parent_id,
			brand_id,
			created_at,
			updated_at
		FROM "category"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		name      sql.NullString
		parent_id sql.NullString
		brand_id  sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&parent_id,
		&brand_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &product_service.Category{
		Id:        id.String,
		Name:      name.String,
		ParentId:  parent_id.String,
		BrandId:   brand_id.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *categoryRepo) GetAll(ctx context.Context, req *product_service.GetListCategoryRequest) (resp *product_service.GetListCategoryResponse, err error) {

	resp = &product_service.GetListCategoryResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			parent_id,
			brand_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "category"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if req.SearchName != "" {
		filter += ` AND name ILIKE '%' || '` + req.SearchName + `' || '%'`
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			parent_id sql.NullString
			brand_id  sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&parent_id,
			&brand_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Categorys = append(resp.Categorys, &product_service.Category{
			Id:        id.String,
			Name:      name.String,
			ParentId:  parent_id.String,
			BrandId:   brand_id.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *categoryRepo) Update(ctx context.Context, req *product_service.UpdateCategory) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "category"
			SET
				name = :name,
				parent_id = :parent_id,
				brand_id = :brand_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"name":      req.GetName(),
		"parent_id": req.GetParentId(),
		"brand_id":  req.GetBrandId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *categoryRepo) Delete(ctx context.Context, req *product_service.CategoryPrimaryKey) (*product_service.CategoryEmpty, error) {

	query := `DELETE FROM "category" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &product_service.CategoryEmpty{}, nil
}
