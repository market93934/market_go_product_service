package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_product_service/genproto/product_service"
	"gitlab.com/market93934/market_go_product_service/pkg/helper"
)

type brandRepo struct {
	db *pgxpool.Pool
}

func NewBrandRepo(db *pgxpool.Pool) *brandRepo {
	return &brandRepo{
		db: db,
	}
}

func (c *brandRepo) Create(ctx context.Context, req *product_service.CreateBrand) (resp *product_service.BrandPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "brand" (
			id,
			photo,
			name
			) VALUES ($1, $2, $3)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Photo,
		req.Name,
	)
	if err != nil {
		return nil, err
	}

	return &product_service.BrandPrimaryKey{Id: id.String()}, nil

}

func (c *brandRepo) GetByPKey(ctx context.Context, req *product_service.BrandPrimaryKey) (resp *product_service.Brand, err error) {
	query := `
		SELECT
			id,
			photo,
			name,
			created_at,
			updated_at
		FROM "brand"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		photo     sql.NullString
		name      sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&photo,
		&name,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &product_service.Brand{
		Id:        id.String,
		Photo:     photo.String,
		Name:      name.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *brandRepo) GetAll(ctx context.Context, req *product_service.GetListBrandRequest) (resp *product_service.GetListBrandResponse, err error) {

	resp = &product_service.GetListBrandResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			photo,
			name,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "brand"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if req.SearchName != "" {
		filter += ` AND name ILIKE '%' || '` + req.SearchName + `' || '%'`
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			photo     sql.NullString
			name      sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&photo,
			&name,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Brands = append(resp.Brands, &product_service.Brand{
			Id:        id.String,
			Photo:     photo.String,
			Name:      name.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *brandRepo) Update(ctx context.Context, req *product_service.UpdateBrand) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "brand"
			SET
				photo = :photo,
				name = :name,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":    req.GetId(),
		"photo": req.GetPhoto(),
		"name":  req.GetName(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *brandRepo) Delete(ctx context.Context, req *product_service.BrandPrimaryKey) (*product_service.BrandEmpty, error) {

	query := `DELETE FROM "brand" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &product_service.BrandEmpty{}, nil
}
