package postgres

import (
	"context"
	"database/sql"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_product_service/genproto/product_service"
	"gitlab.com/market93934/market_go_product_service/pkg/helper"
)

type remainderRepo struct {
	db *pgxpool.Pool
}

func NewRemainderRepo(db *pgxpool.Pool) *remainderRepo {
	return &remainderRepo{
		db: db,
	}
}

func (c *remainderRepo) Create(ctx context.Context, req *product_service.CreateReminder) (resp *product_service.ReminderPrimaryKey, err error) {
	query := `INSERT INTO "reminder" (
			branch_id,
			category_id,
			brand_id,
			product_id,
			bar_code,
			count,
			price
			) VALUES ($1, $2, $3, $4, $5, $6, $7)
	`
	_, err = c.db.Exec(ctx,
		query,
		req.BranchId,
		req.CategotyId,
		req.BrandId,
		req.ProductId,
		req.BarCode,
		req.Count,
		req.Price,
	)
	if err != nil {
		return nil, err
	}

	return &product_service.ReminderPrimaryKey{ProductId: req.ProductId}, nil

}

func (c *remainderRepo) GetByPKey(ctx context.Context, req *product_service.ReminderPrimaryKey) (resp *product_service.Reminder, err error) {
	query := `
		SELECT
			product_id,
			branch_id,
			category_id,
			brand_id,
			bar_code,
			count,
			price,
			created_at,
			updated_at
		FROM "reminder"
		WHERE product_id = $1
	`

	var (
		product_id  sql.NullString
		branch_id   sql.NullString
		category_id sql.NullString
		brand_id    sql.NullString
		bar_code    sql.NullString
		count       int
		price       float64
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.ProductId).Scan(
		&product_id,
		&branch_id,
		&category_id,
		&brand_id,
		&bar_code,
		&count,
		&price,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &product_service.Reminder{
		ProductId:  product_id.String,
		BranchId:   branch_id.String,
		CategotyId: category_id.String,
		BrandId:    brand_id.String,
		BarCode:    bar_code.String,
		Count:      int64(count),
		Price:      price,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *remainderRepo) GetAll(ctx context.Context, req *product_service.GetListReminderRequest) (resp *product_service.GetListReminderResponse, err error) {

	resp = &product_service.GetListReminderResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			branch_id,
			category_id,
			brand_id,
			product_id,
			bar_code,
			count,
			price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "reminder"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if req.BarCode != "" {
		filter += " AND bar_code ILIKE '%' || '" + req.BarCode + "' || '%'"
	}
	if req.BranchId != "" {
		filter += " AND branch_id ILIKE '%' || '" + req.BranchId + "' || '%'"
	}
	if req.SearchBrandId != "" {
		filter += ` AND brand_id ILIKE '%' || '` + req.SearchBrandId + `' || '%'`
	}
	if req.SearchCategory != "" {
		filter += ` AND category_id ILIKE '%' || '` + req.SearchCategory + `' || '%'`
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			branch_id   sql.NullString
			category_id sql.NullString
			brand_id    sql.NullString
			product_id  sql.NullString
			bar_code    sql.NullString
			count       int
			price       float64
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&branch_id,
			&category_id,
			&brand_id,
			&product_id,
			&bar_code,
			&count,
			&price,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Reminders = append(resp.Reminders, &product_service.Reminder{
			BranchId:   branch_id.String,
			CategotyId: category_id.String,
			BrandId:    brand_id.String,
			ProductId:  product_id.String,
			BarCode:    bar_code.String,
			Count:      int64(count),
			Price:      price,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}

	return
}

func (c *remainderRepo) Update(ctx context.Context, req *product_service.UpdateReminder) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "reminder"
			SET
				branch_id = :branch_id,
				category_id = :category_id,
				brand_id = :brand_id,
				product_id =:product_id,
				bar_code = :bar_code,
				count = :count,
				price = :price,
				updated_at = now()
			WHERE
				product_id = :product_id`
	params = map[string]interface{}{
		"branch_id":   req.GetBranchId(),
		"category_id": req.GetCategotyId(),
		"brand_id":    req.GetBrandId(),
		"product_id":  req.GetProductId(),
		"bar_code":    req.GetBarCode(),
		"count":       req.GetCount(),
		"price":       req.GetPrice(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *remainderRepo) Delete(ctx context.Context, req *product_service.ReminderPrimaryKey) (*product_service.ReminderEmpty, error) {

	query := `DELETE FROM "reminder" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.ProductId)

	if err != nil {
		return nil, err
	}

	return &product_service.ReminderEmpty{}, nil
}
