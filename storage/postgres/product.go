package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market93934/market_go_product_service/genproto/product_service"
	"gitlab.com/market93934/market_go_product_service/pkg/helper"
)

type productRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) *productRepo {
	return &productRepo{
		db: db,
	}
}

func (c *productRepo) Create(ctx context.Context, req *product_service.CreateProduct) (resp *product_service.ProductPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "product" (
			id,
			photo,
			name,
			category_id,
			brand_id,
			bar_code,
			price
			) VALUES ($1, $2, $3, $4, $5, $6, $7)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Photo,
		req.Name,
		req.CategotyId,
		req.BrandId,
		req.BarCode,
		req.Price,
	)
	if err != nil {
		return nil, err
	}

	return &product_service.ProductPrimaryKey{Id: id.String()}, nil

}

func (c *productRepo) GetByPKey(ctx context.Context, req *product_service.ProductPrimaryKey) (resp *product_service.Product, err error) {
	query := `
		SELECT
			id,
			photo,
			name,
			category_id,
			brand_id,
			bar_code,
			price,
			created_at,
			updated_at
		FROM "product"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		photo       sql.NullString
		name        sql.NullString
		category_id sql.NullString
		brand_id    sql.NullString
		bar_code    sql.NullString
		price       float64
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&photo,
		&name,
		&category_id,
		&brand_id,
		&bar_code,
		&price,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &product_service.Product{
		Id:         id.String,
		Photo:      photo.String,
		Name:       name.String,
		CategotyId: category_id.String,
		BrandId:    brand_id.String,
		BarCode:    bar_code.String,
		Price:      price,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *productRepo) GetAll(ctx context.Context, req *product_service.GetListProductRequest) (resp *product_service.GetListProductResponse, err error) {

	resp = &product_service.GetListProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			photo,
			name,
			category_id,
			brand_id,
			bar_code,
			price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "product"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	if len(req.BarCode) > 0 {
		filter += ` AND bar_code ILIKE '%' || '` + req.BarCode + `' || '%'`
	}
	if req.SearchName != "" {
		filter += ` AND name ILIKE '%' || '` + req.SearchName + `' || '%'`
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			photo       sql.NullString
			name        sql.NullString
			category_id sql.NullString
			brand_id    sql.NullString
			bar_code    sql.NullString
			price       float64
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&photo,
			&name,
			&category_id,
			&brand_id,
			&bar_code,
			&price,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Products = append(resp.Products, &product_service.Product{
			Id:         id.String,
			Photo:      photo.String,
			Name:       name.String,
			CategotyId: category_id.String,
			BrandId:    brand_id.String,
			BarCode:    bar_code.String,
			Price:      price,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}

	return
}

func (c *productRepo) Update(ctx context.Context, req *product_service.UpdateProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "product"
			SET
				photo = :photo,
				name = :name,
				category_id = :category_id,
				brand_id = :brand_id,
				bar_code = :bar_code,
				price = :price,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"photo":       req.GetPhoto(),
		"name":        req.GetName(),
		"category_id": req.GetCategotyId(),
		"brand_id":    req.GetBrandId(),
		"bar_code":    req.GetBarCode(),
		"price":       req.GetPrice(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *productRepo) Delete(ctx context.Context, req *product_service.ProductPrimaryKey) (*product_service.ProductEmpty, error) {

	query := `DELETE FROM "product" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &product_service.ProductEmpty{}, nil
}
